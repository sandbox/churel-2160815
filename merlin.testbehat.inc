<?php
/**
 * @file
 * All function for testing components.
 */

/**
 * Creation of the behat test of content type.
 */
function merlin_test_content_type($row) {
  return theme('merlin_test_content_type', array('row' => $row));
}
/**
 * Creation of the behat test of field.
 */
function merlin_test_field($row) {
  return theme('merlin_test_field', array('row' => $row));
}
/**
 * Creation of the behat test of taxonomy.
 */
function merlin_test_taxonomy($row) {
  return theme('merlin_test_taxonomy', array('row' => $row));
}
/**
 * Return type form for a widget.
 */
function merlin_test_form_type($widget) {
  $types = array(
    'Autocomplete term widget (tagging)' => 'text',
    'Check boxes/radio buttons' => 'checkbox',
    'Email field' => 'textfield',
    'Embedded' => 'textfield',
    'File'  => 'managed-file',
    'Image' => 'managed-file',
    'Inline Entity Form - Multiple values' => 'textarea',
    'Inline Entity Form - Single value' => 'textarea',
    'Number field' => 'textfield',
    'Pop-up calendar' => 'textfield',
    'Select list' => 'select',
    'Single on/off checkbox' => 'checkbox',
    'Text area (multiple rows)' => '',
    'Text area with a summary' => '',
    'Text field' => 'textfield',
    'Time period (select)' => 'select',
    'Hidden' => '',
    'Range field' => 'textfield',
    'Link' => 'textfield',
    'URL field' => 'textfield',
    'Location Field' => 'textfield',
    'Name' => 'textfield',
    'Static' => 'textfield',
    'Autocomplete term widget (tagging)' => 'textfield',
    'YouTube' => 'textfield',
  );
  return $types[$widget];
}
