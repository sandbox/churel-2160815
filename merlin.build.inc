<?php
/**
 * @file
 * File to store every function for building.
 */

/**
 * Function to create layout field.
 *
 * @param array $row
 *   Info about the taxonomy.
 *
 * @return string
 *   A Gherkin test to create a taxonomy.
 */
function merlin_create_layout_fields(array $row) {
  if ($row['Field'] != 'title' && $row['Field'] != 'body') {
    $row['field_machine'] = 'field_' . merlin_to_machine_name($row['Field']);
  }
  else {
    $row['field_machine'] = merlin_to_machine_name($row['Field']);
  }
  return theme('merlin_layout_fields', array('row' => $row));
}
/**
 * Function to create a field_group.
 *
 * @param array $row
 *   Info about the taxonomy.
 *
 * @return string
 *   A Gherkin test to create a taxonomy.
 */
function merlin_create_field_group(array $row) {
  return theme('merlin_field_group', array('row' => $row));
}
/**
 * Function to create field.
 *
 * @param array $row
 *   Info about the taxonomy.
 *
 * @return string
 *   A Gherkin test to create a taxonomy
 */
function merlin_create_field(array $row) {
  return theme('merlin_field', array('row' => $row));
}
/**
 * Function to create feature.
 *
 * @param array $row
 *   Info about the taxonomy.
 *
 * @return string
 *   A Gherkin test to create a taxonomy
 */
function merlin_create_feature(array $row) {
  return theme('merlin_feature', array('row' => $row));
}

/**
 * Function to create a taxonomy.
 *
 * @param array $row
 *   Info about the taxonomy.
 *
 * @return string
 *   A Gherkin test to create a taxonomy
 */
function merlin_create_taxonomy(array $row) {
  return theme('merlin_taxonomy', array('row' => $row));
}
/**
 * Function to create layout.
 *
 * @param array $row
 *   Info about the taxonomy.
 *
 * @return string
 *   A Gherkin test to create a taxonomy.
 */
function merlin_create_layout(array $row) {
  return theme('merlin_layout', array('row' => $row));
}
/**
 * Function to create view mode.
 *
 * @param array $row
 *   Info about the taxonomy.
 *
 * @return string
 *   A Gherkin test to create a taxonomy
 */
function merlin_create_view_mode(array $row) {
  $bundles = $row;
  unset($bundles['Label']);
  unset($bundles['Feature']);

  return theme('merlin_view_mode',
    array(
      'view_mode_name' => $row['Label'],
      'bundles' => $bundles,
    )
  );
}
/**
 * Function to create content type.
 *
 * @param array $row
 *   Info about the taxonomy.
 *
 * @return string
 *   A Gherkin test to create a taxonomye
 */
function merlin_create_content_type(array $row) {
  return theme('merlin_content_type', array('row' => $row));
}
