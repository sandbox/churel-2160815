<?php
/**
 * @file
 * Ods-php a library to read and write ods files from php.
 *
 * This library has been forked from eyeOS project and licended under the LGPL3
 * terms available at: http://www.gnu.org/licenses/lgpl-3.0.txt (relicenced
 * with permission of the copyright holders)
 *
 * https://sourceforge.net/projects/ods-php/.
 */

class MerlinOds {
  public $fonts;
  public $styles;
  public $sheets;
  public $lastElement;
  public $fods;
  public $currentSheet;
  public $currentRow;
  public $currentCell;
  public $lastRowAtt;
  public $repeat;

  /**
   * Constructor.
   */
  public function __construct() {
    $this->styles = array();
    $this->fonts = array();
    $this->sheets = array();
    $this->currentRow = 0;
    $this->currentSheet = 0;
    $this->currentCell = 0;
    $this->repeat = 0;
  }
  /**
   * Parse the file.
   */
  public function parse($data) {
    $xml_parser = xml_parser_create();
    xml_set_object($xml_parser, $this);
    xml_set_element_handler($xml_parser, "startElement", "endElement");
    xml_set_character_data_handler($xml_parser, "characterData");
    xml_parse($xml_parser, $data, strlen($data));
    xml_parser_free($xml_parser);
  }

  /**
   * Start Element.
   *
   *   See (http://www.php.net/manual/en/function.xml-set-element-handler.php).
   */
  public function startElement($parser, $tagname, $attrs) {
    $ctagname = strtolower($tagname);
    if ($ctagname == 'table:table-cell') {
      $this->lastElement = $ctagname;
      if (isset($attrs['TABLE:NUMBER-COLUMNS-REPEATED'])) {
        $times = intval($attrs['TABLE:NUMBER-COLUMNS-REPEATED']);
        $times--;
        $this->currentCell += $times;
        $this->repeat = $times;
      }
      if (isset($this->lastRowAtt['TABLE:NUMBER-ROWS-REPEATED'])) {
        $times = intval($this->lastRowAtt['TABLE:NUMBER-ROWS-REPEATED']);
        $times--;
        $this->currentRow += $times;
      }
    }
    elseif ($ctagname == 'table:table-row') {
      $this->lastRowAtt = $attrs;
    }
    elseif ($ctagname == 'table:table') {
      $this->currentSheet = $attrs['TABLE:NAME'];
    }
  }
  /**
   * End Element.
   *
   *   See (http://www.php.net/manual/en/function.xml-set-element-handler.php).
   */
  public function endElement($parser, $tagname) {
    $ctagname = strtolower($tagname);
    if ($ctagname == 'table:table') {
      $this->currentRow = 0;
    }
    elseif ($ctagname == 'table:table-row') {
      $this->currentRow++;
      $this->currentCell = 0;
    }
    elseif ($ctagname == 'table:table-cell') {
      $this->currentCell++;
      $this->repeat = 0;
    }
  }
  /**
   * Parse a data.
   */
  public function characterData($parser, $data) {
    if ($this->lastElement == 'table:table-cell') {
      if ($this->currentRow > 0) {
        $name = $this->sheets[$this->currentSheet][0][$this->currentCell];
        $this->sheets[$this->currentSheet][$this->currentRow][$name] = $data;
      }
      else {
        $this->sheets[$this->currentSheet][$this->currentRow][$this->currentCell] = $data;
      }

      if ($this->repeat > 0) {
        for ($i = 0; $i < $this->repeat; $i++) {
          $cnum = $this->currentCell - ($i + 1);
          if ($this->currentRow > 0) {
            $name = $this->sheets[$this->currentSheet][0][$cnum];
            $this->sheets[$this->currentSheet][$this->currentRow][$name] = $data;
          }
          else {
            $this->sheets[$this->currentSheet][$this->currentRow][$cnum] = $data;
          }
        }
      }
    }
  }
}
/**
 * Parse a ODS file.
 */
function merlin_parseOds($file) {
  $tmp = merlin_get_tmp_dir();
  copy($file, $tmp . '/' . basename($file));
  $path = $tmp . '/' . basename($file);
  $uid = uniqid();
  mkdir($tmp . '/' . $uid);
  shell_exec('unzip ' . escapeshellarg($path) . ' -d ' . escapeshellarg($tmp . '/' . $uid));
  $obj = new MerlinOds();
  $obj->parse(file_get_contents($tmp . '/' . $uid . '/content.xml'));
  return $obj;
}
/**
 * Get the tpm directory.
 */
function merlin_get_tmp_dir() {
  $path = '';
  if (!function_exists('sys_get_temp_dir')) {
    $path = merlin_try_get_temp_dir();
  }
  else {
    $path = sys_get_temp_dir();
    if (is_dir($path)) {
      return $path;
    }
    else {
      $path = merlin_try_get_temp_dir();
    }
  }
  return $path;
}
/**
 * Get the tpm directory.
 */
function merlin_try_get_temp_dir() {
  // Try to get from environment variable.
  if (!empty($_ENV['TMP'])) {
    $path = realpath($_ENV['TMP']);
  }
  elseif (!empty($_ENV['TMPDIR'])) {
    $path = realpath($_ENV['TMPDIR']);
  }
  elseif (!empty($_ENV['TEMP'])) {
    $path = realpath($_ENV['TEMP']);
  }
  // Detect by creating a temporary file.
  else {
    // Try to use system's temporary directory.
    // as random name shouldn't exist.
    $temp_file = tempnam(md5(uniqid(rand(), TRUE)), '');
    if ($temp_file) {
      $temp_dir = realpath(dirname($temp_file));
      unlink($temp_file);
      $path = $temp_dir;
    }
    else {
      return "/tmp";
    }
  }
  return $path;
}
