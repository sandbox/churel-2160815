<?php
/**
 * Copy this fils into you behat directory to had some test case.
 */

use Behat\Behat\Context\ClosuredContextInterface,
Behat\Behat\Context\TranslatedContextInterface,
Behat\Behat\Context\BehatContext,
Behat\Behat\Exception\PendingException;
use Behat\Gherkin\Node\PyStringNode,
Behat\Gherkin\Node\TableNode;
use Behat\MinkExtension\Context\MinkContext;
use Drupal\Component\Utility\Random;

/**
 * Features context.
 */
class FeatureContext extends Behat\Behat\Context\BehatContext {
  /**
   * Wait x second.
   *
   * @Given /^I wait "([^"]*)" sec$/
   */
  public function iWaitSec($arg1) {
    $sec = $arg1 * 1000;
    $this->getSession()->wait($sec, '1 == 0');
  }
}
