Merlin
=============================

https://drupal.org/sandbox/churel/2160815

Feature
-------------

From a template spreadsheet, the wizard can automatically build
components on your Drupal website.
Components supported by the wand :
-Features
-Content type
-Fields
-Field group
-Taxonomy
-View modes
In addition, 
Merlin provides Gherkin automated tests for all components he build. 
This module is for development sites not for production site!

How to install Merlin
---------------------------------------
1. Install Merlin (unpacking it to your Drupal
/sites/all/modules directory if you're installing by hand, for example).

2. Enable Merlin module in Admin menu > Modules.

3. If you are not familiar with Behat and selenium,
you should start reading this awesome article by Lin Clark :
http://lin-clark.com/blog/2013/11/26/quickstart-testing-with-behat-mink-selenium
She explain what is behat, how to install it and how to use it.
You will need to do : 
   -Install Behat (http://behat.org/)
   -Get Selenium (http://docs.seleniumhq.org/download/). 
On a stand alone version 
selenium-release.storage.googleapis.com/2.42/selenium-server-standalone-2.42.2.jar
   -Run selenium java -jar selenium-server-standalone-2.42.2.jar

How to interact with Merlin
---------------------------------------
-Download and enable merlin
-Clone and fill the template spreadsheet (in the ods-template folder) 
  with all the components your website need
-Give it to merlin (admin/config/system/merlin)
-Give to merlin the right credential for creating components
 (most of the time admin credential)

How to build your Site
---------------------------------------
-Merlin will give you a Gherkin suite 
and some new steps for your behat environment.
-Copy the FeatureContext.php into your Features folder on behat
-Copy the building.feature into your Features folder on behat
-Run behat (for example bin/behat Features/building.feature)

How to test your Site
---------------------------------------
-You will need to install Behat Drupal Extension
(https://drupal.org/project/drupalextension)
-Copy the FeatureContext.php into your Features folder on behat
-Copy every feature file into your Features folder on behat
-Run behat (for example bin/behat Features/building.feature)


Presentation
-------------
I presented this module on the DrupalCamp Toronto 2014
https://www.youtube.com/watch?v=EyINJdXY35Y

Blog Post
---------
I wrote a blog article about it : http://therefore.ca/blog/birth-merlin
