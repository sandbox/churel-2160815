<?php
/**
 * @file
 * Map ods file.
 */

/**
 * Map Ods Sheet with a array.
 *
 * @param array $ods
 *   The ods file.
 *
 * @return array
 *   A array formatted.
 */
function merlin_map_array(array $ods) {
  $result = array();
  foreach ($ods as $key => $sheet) {
    switch ($key) {
      case 'Features':
        merlin_map_feature($sheet, $result);
        break;

      case 'View modes':
        merlin_map_view_mode($sheet, $result);
        break;

      case 'Content Types':
        merlin_map_content_type($sheet, $result);
        break;

      case 'Content Type Fields Field Group':
        merlin_map_field_group($sheet, $result);
        break;

      case 'Content Type Fields':
        merlin_map_field($sheet, $result);
        break;

      case 'Content Type Layout':
        merlin_map_layout($sheet, $result);
        break;

      case 'Content Type Display Field Group':
        merlin_map_display_field_group($sheet, $result);
        break;

      case 'Content Type Display':
        merlin_map_layout_fields($sheet, $result);
        break;

      case 'Taxonomy':
        merlin_map_taxonomy($sheet, $result);
        break;

    }
  }
  return $result;

}
/**
 * Map field layout.
 *
 * @param array $sheet
 *   The ods sheet converted as array.
 * @param array $result
 *   Array with all the data parsed.
 */
function merlin_map_layout_fields(array $sheet, array &$result) {
  static $content_type, $view_mode;
  foreach ($sheet as $key => $row) {
    if (merlin_verify_row($key, $row)) {
      if (!empty($row['Content Type'])) {
        $content_type = $row['Content Type'];
        $view_mode = $row['View mode'];
      }
      else {
        $row['Content Type'] = $content_type;
        $row['View mode'] = $view_mode;
      }
      $result['layout_fields'][] = $row;
    }

  }
}
/**
 * Map field_group.
 *
 * @param array $sheet
 *   The ods sheet converted as array.
 * @param array $result
 *   Array with all the data parsed.
 */
function merlin_map_field_group(array $sheet, array &$result) {
  static $content_type;
  foreach ($sheet as $key => $row) {
    if (merlin_verify_row($key, $row)) {
      if (!empty($row['Content Type'])) {
        $content_type = $row['Content Type'];
      }
      else {
        $row['Content Type'] = $content_type;
      }
      if (isset($row['Feature'])) {
        $feature_machine_name = merlin_to_machine_name($row['Feature']);
        $machine_name = merlin_to_machine_name($row['Label']);
        $result['feature'][$feature_machine_name]['field_group'][$machine_name] = $row['Label'];
      }
      $result['field_group'][] = $row;
    }
  }
}
/**
 * Map layout.
 *
 * @param array $sheet
 *   The ods sheet converted as array.
 * @param array $result
 *   Array with all the data parsed.
 */
function merlin_map_layout(array $sheet, array &$result) {
  foreach ($sheet as $key => $row) {
    if (merlin_verify_row($key, $row)) {
      if (isset($row['Feature'])) {
        $feature_machine_name = merlin_to_machine_name($row['Feature']);
        $machine_name = merlin_to_machine_name($row['Content Type']);
        $result['feature'][$feature_machine_name]['content_type_layout'][$machine_name] = array('view_mode' => merlin_to_machine_name($row['View mode']), 'content_type' => merlin_to_machine_name($row['Content Type']));
      }
      $result['layout'][] = $row;
    }
  }
}
/**
 * Map field_group_display.
 *
 * @param array $sheet
 *   The ods sheet converted as array.
 * @param array $result
 *   Array with all the data parsed.
 */
function merlin_map_display_field_group(array $sheet, array &$result) {
  foreach ($sheet as $key => $row) {
    if (merlin_verify_row($key, $row)) {
      $result['field_group_display'][] = $row;
    }
  }
}
/**
 * Manage Field.
 *
 * @param array $sheet
 *   The ods sheet converted as array.
 * @param array $result
 *   Array with all the data parsed.
 */
function merlin_map_field(array $sheet, array &$result) {
  static $content_type;
  foreach ($sheet as $key => $row) {
    if (!empty($row['Content Type'])) {
      $content_type = $row['Content Type'];
    }
    else {
      $row['Content Type'] = $content_type;
    }
    if (merlin_verify_row($key, $row)) {
      $result['field'][] = $row;
    }
  }
}
/**
 * Map taxonomy.
 *
 * @param array $sheet
 *   The ods sheet converted as array.
 * @param array $result
 *   Array with all the data parsed.
 */
function merlin_map_taxonomy(array $sheet, array &$result) {
  foreach ($sheet as $key => $row) {
    if (merlin_verify_row($key, $row)) {
      if (isset($row['Feature'])) {
        $feature_machine_name = merlin_to_machine_name($row['Feature']);
        $machine_name = merlin_to_machine_name($row['Name']);
        $result['feature'][$feature_machine_name]['taxonomy'][$machine_name] = $row['Name'];
      }
      $result['taxonomy'][] = $row;
    }
  }
}
/**
 * Manage Feature Sheet.
 *
 * @param array $sheet
 *   The ods sheet converted as array.
 * @param array $result
 *   Array with all the data parsed.
 */
function merlin_map_feature(array $sheet, array &$result) {
  foreach ($sheet as $key => $row) {
    if (merlin_verify_row($key, $row)) {
      $machine_name = merlin_to_machine_name($row['Feature Name']);
      $result['feature'][$machine_name] = $row;
    }
  }
}
/**
 * Manage View Mode Sheet.
 *
 * @param array $sheet
 *   The ods sheet converted as array.
 * @param array $result
 *   Array with all the data parsed.
 */
function merlin_map_view_mode(array $sheet, array &$result) {
  foreach ($sheet as $key => $row) {
    if (merlin_verify_row($key, $row)) {

      if (isset($row['Feature'])) {
        $feature_machine_name = merlin_to_machine_name($row['Feature']);
        $machine_name = merlin_to_machine_name($row['Label']);
        $result['feature'][$feature_machine_name]['view_mode'][$machine_name] = $row['Label'];
      }
      $result['view_mode'][] = $row;
    }
  }
}
/**
 * Manage Content type Sheet.
 *
 * @param array $sheet
 *   The ods sheet converted as array.
 * @param array $result
 *   Array with all the data parsed.
 */
function merlin_map_content_type(array $sheet, array &$result) {
  foreach ($sheet as $key => $row) {
    if (merlin_verify_row($key, $row)) {
      // Manage Feature.
      if (isset($row['Feature'])) {
        $feature_machine_name = merlin_to_machine_name($row['Feature']);
        $machine_name = merlin_to_machine_name($row['Name']);
        $result['feature'][$feature_machine_name]['content_type'][$machine_name] = $row['Name'];
      }
      $result['content_type'][] = $row;
    }
  }
}
/**
 * Generic function to verify a row.
 *
 * @param int $key
 *   Key of the row.
 * @param array $row
 *   The row.
 */
function merlin_verify_row($key, array $row) {
  if ($key == 0) {
    return FALSE;
  }
  elseif (isset($row['Ignore']) && $row['Ignore'] == 1) {
    return FALSE;
  }
  else {
    return TRUE;
  }
}
