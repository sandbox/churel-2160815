<?php
/**
 * @file
 * Behat scenario to test a Field.
 */
?>
<?php if(merlin_test_form_type($row['Widget'])) :?>
	
	
	@javascript
	Scenario: Test the Field <?php print $row['Label']?>	
		Given I am on "node/add/<?php print merlin_machine_name_to_url($row['Content Type'])?>"
		Then I should see a "<?php print $row['Label']?>" <?php print merlin_test_form_type($row['Widget'])?> form element
<?php endif?>
