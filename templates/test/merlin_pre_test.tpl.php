<?php
/**
 * @file
 * Behat scenario to test a feature.
 */
?>
 Feature: Test For <?php print $feature?> Feature
	Background: 
	   Given I am on "/user" 
	   When I fill in "name" with "<?php print $username?>"
	   And I fill in "pass" with "<?php print $pass?>"
	   And I press "Log in"
