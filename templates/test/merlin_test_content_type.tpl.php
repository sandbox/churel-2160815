<?php
/**
 * @file
 * Behat scenario to test a content type.
 */
?>

	@javascript
	Scenario: Test content type <?php print $row['Name'] ?> 
	    Given I am on "/node/add/<?php print merlin_machine_name_to_url($row['Name'])?>" 
	    Then I should see "Create <?php print ($row['Name']) ?>" 
