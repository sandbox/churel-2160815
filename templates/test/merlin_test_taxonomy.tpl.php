<?php
/**
 * @file
 * Behat scenario to test a taxonomy vocabulary.
 */
?>

	@javascript
	Scenario: Test vocabulary <?php print $row['Name'] ?> 
	    Given I am on "admin/structure/taxonomy" 
	    Then I should see "<?php print ($row['Name']) ?>" 
