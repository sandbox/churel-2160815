<?php
/**
 * @file
 * Behat scenario to build a view_mode.
 */
?>

	@javascript
	Scenario: Creation of View mode <?php print $view_mode_name ?> 
	   Given I am on "/admin/structure/ds/view_modes/manage" 
	   When I fill in "Label" with "<?php print $view_mode_name?>"
<?php foreach($bundles as $bundle => $bool) : ?>
<?php if($bool): ?>
	  	And I check "<?php print $bundle?>"
<?php endif?>
<?php endforeach?>
	    And I press "Save"
