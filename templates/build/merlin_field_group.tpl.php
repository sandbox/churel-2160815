<?php
/**
 * @file
 * Behat scenario to build a field group.
 */
?>

	@javascript
   	Scenario: Create Field group <?php print $row['Label']?>

   		Given I am on "admin/structure/types/manage/<?php print merlin_to_machine_name($row['Content Type'])?>/fields"
   		When I fill in "fields[_add_new_group][label]" with "<?php print $row['Label']?>"
   		And I follow "Show row weights"
<?php if(isset($row['Weight'])) :?>
		And fill in "fields[_add_new_group][weight]" with "<?php print $row['Weight']?>"
<?php endif?>
	  	And fill in "fields[_add_new_group][group_name]" with "<?php print merlin_to_machine_name($row['Label'])?>"
<?php if(isset($row['Widget'])) :?>
		And I select "<?php print $row['Widget']?>" from "fields[_add_new_group][format][type]"
<?php endif?>
	  	And I press "Save"
