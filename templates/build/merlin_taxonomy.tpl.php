<?php
/**
 * @file
 * Behat scenario to build a taxonomy vocabulary.
 */
?>

	@javascript
	Scenario: Create vocabulary <?php print $row['Name'] ?> 
	    Given I am on "/admin/structure/taxonomy/add" 
	    When I fill in "Name" with "<?php print $row['Name'] ?>" 
<?php if(!empty($row['Description'])) : ?>
	    When I fill in "Description" with "<?php print $row['Description'] ?>"
<?php endif?>
	    And I press "Save" 
