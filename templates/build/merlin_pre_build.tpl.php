<?php
/**
 * @file
 * Behat Feature.
 */
?>
Feature: Build Your awesome website
	Background: 
	   Given I am on "/user" 
	   When I fill in "name" with "<?php print $username?>"
	   And I fill in "pass" with "<?php print $pass?>"
	   And I press "Log in"
