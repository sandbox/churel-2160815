<?php
/**
 * @file
 * Behat scenario to build a field.
 */
?>

	@javascript
	Scenario: Create the Field <?php print $row['Label']?>	
		Given I am on "admin/structure/types/manage/<?php print merlin_to_machine_name($row['Content Type'])?>/fields"
		When I fill in "fields[_add_new_field][label]" with "<?php print $row['Label']?>"
		And I follow "Show row weights"
<?php if(!empty($row['Field type'])) : ?>
		And I select "<?php print $row['Field type']?>" from "fields[_add_new_field][type]"
<?php endif?>
<?php if(!empty($row['Widget'])) :?>
		And I select "<?php print $row['Widget']?>" from "fields[_add_new_field][widget_type]"
<?php endif;?>
<?php if(!empty($row['Parent'])) :?>
		And I select "<?php print $row['Parent']?>" from "fields[_add_new_field][parent]"
<?php endif;?>
<?php if(!empty($row['Weight'])) :?>
		And I fill in "fields[_add_new_group][weight]" with "<?php print $row['Weight']?>"
<?php endif;?>
		And I press "Save"
