<?php
/**
 * @file
 * Behat scenario to build a feature.
 */
?>

	@javascript
   	Scenario: Create Feature <?php print $row['Feature Name'] ?> 
	  Given I am on "admin/structure/features/create"
	  When I fill in "edit-name" with "<?php print $row['Feature Name'] ?>"
<?php if(!empty($row['Description'])) : ?>
	    When I fill in "Description" with "<?php print $row['Description']?>"
<?php endif?>
<?php if(isset($row['content_type'])) :?>
	  	And I click "Content type"
<?php foreach($row['content_type'] as $content_type) : ?>
	  	And I check "<?php print $content_type?>"
<?php endforeach?>
	  	And I click "Content type"
<?php endif?>
<?php if(isset($row['view_mode'])): ?>
	  	And I click "Display Suite view modes"
<?php foreach($row['view_mode'] as $view_mode) : ?>
	  	And I check "ds_view_modes[sources][selected][<?php print merlin_to_machine_name($view_mode)?>]"
<?php endforeach?>
	  	And I click "Display Suite view modes"
<?php endif?>
<?php if(isset($row['content_type_layout'])): ?>
	  	And I click "Display Suite layout settings"
<?php foreach($row['content_type_layout'] as $layout) : ?>
	  	And I check "ds_layout_settings[sources][selected][node|<?php print $layout['content_type']?>|<?php print $layout['view_mode']?>]"
<?php endforeach?>
	  	And I click "Display Suite layout settings"
<?php endif?>
<?php if(isset($row['field_group'])): ?>
	  	And I click "Fieldgroup"
<?php foreach($row['field_group'] as $field_group) : ?>
	  	And I check "<?php print 'group_' . merlin_to_machine_name($field_group)?>"
<?php endforeach?>
	  	And I click "Fieldgroup"
<?php endif?>
<?php if(isset($row['taxonomy'])): ?>
	  	And I click "Taxonomy"
<?php foreach($row['taxonomy'] as $taxonomy) : ?>
	  	And I check "taxonomy[sources][selected][<?php print merlin_to_machine_name($taxonomy)?>]"
<?php endforeach?>
	  	And I click "Taxonomy"
<?php endif?> 	
<?php if(!empty($row['Version'])): ?>
	  	And I fill in "Version" with "<?php print $row['Version']?>"
<?php endif?>
		And I click "Advanced Options"
<?php if(!empty($row['Path to Generate feature module'])): ?>
	  	And I fill in "Path to Generate feature module" with "<?php print $row['Path to Generate feature module']?>"
<?php endif?>
		And I press "Generate feature"
