<?php
/**
 * @file
 * Behat scenario to build a layout field.
 */
?>

	@javascript
	Scenario: Add the fields layout for <?php print $row['View mode']?> view mode and <?php print $row['Content Type']?> content type.
		Given I am on "admin/structure/types/manage/<?php print merlin_to_machine_name($row['Content Type'])?>/display/"
		And I follow "Show row weights"
<?php if(merlin_to_machine_name($row['View mode']) != "default") : ?>
		And I follow "<?php print $row['View mode']?>"
<?php endif?>
<?php if(!empty($row['Weight'])) : ?>
		And I fill in "fields[<?php print $row['field_machine']?>][weight]" with "<?php print $row['Weight']?>"
<?php endif?>
<?php if(!empty($row['Parent'])) : ?>

		And I select "<?php print $row['Parent']?>" from "fields[<?php print $row['field_machine']?>][parent]"
<?php endif?>
<?php if(!empty($row['Region'])) : ?>
		And I select "<?php print $row['Region']?>" from "fields[<?php print $row['field_machine']?>][region]"
		And I wait "2" sec
<?php else : ?>
		And I select "Content" from "fields[<?php print $row['field_machine']?>][region]"
		And I wait "2" sec
<?php endif; ?>
		And I select "<?php print $row['Label']?>" from "fields[<?php print $row['field_machine']?>][label]"
		And I press "Save"
