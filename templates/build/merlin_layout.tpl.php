<?php
/**
 * @file
 * Behat scenario to build a layout.
 */
?>

 	@javascript
	Scenario: Add the layout <?php print $row['View mode']?> for <?php print $row['Content Type']?> 
		Given I am on "admin/structure/types/manage/<?php print merlin_to_machine_name($row['Content Type'])?>/display/" 
<?php if(merlin_to_machine_name($row['View mode']) != "teaser" && merlin_to_machine_name($row['View mode']) != "default") : ?>
		And I click "Custom display settings"
		And I check "<?php print $row['View mode']?>"
		And I press "Save"	
<?php endif;?>
<?php if(merlin_to_machine_name($row['View mode']) != "default") : ?>
		And I click "<?php print $row['View mode']?>"
<?php endif?>
		And I select "<?php print $row['Layout']?>" from "additional_settings[layout]"
		And I wait "2" sec
		And I press "Save"
