<?php
/**
 * @file
 * Behat scenario to build a content type.
 */
?>

	@javascript
	Scenario: Create Content type 
	    Given I am on "/admin/structure/types/add" 
	    When I fill in "Name" with "<?php print $row['Name']?>"
<?php if(!empty($row['Description'])) : ?>
	    When I fill in "Description" with "<?php print $row['Description']?>"
<?php endif?>
	    And I press "Save content type"
